package kr.co.alteration.visitetri.device;

import android.content.SharedPreferences;

/**
 * Created by miffle-laptop on 2016-08-07.
 */
public class CookieManager {

    private static String loginCookie;

    public synchronized static void setLoginCookie(String cookie) {
        loginCookie = cookie;
    }

    public synchronized static String getLoginCookie() {
        return loginCookie;
    }

    public synchronized static void saveToDB() {

    }

    public synchronized static void loadToManager() {

    }
}
