package kr.co.alteration.visitetri.comms;

import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;

/**
 * Created by blogc on 2016-08-21.
 */
public abstract class CommServer extends Thread {
    private HttpURLConnection httpURLConnection;
    private URL url;
    private String parameter;
    private OutputStream outputStream;
    private BufferedReader bufferedReader;

    protected String result;

    private String handlerLocation;
    private int TIMEOUT = 10000;
    private int cycle = -1;
    private ICallback callback;
    private IPostwork postwork;

    private String ipAddress = ServerInfos.SERVER_ADDRESS;
    private String port = String.valueOf(ServerInfos.SERVER_PORT);

    private boolean noOutput = false;

    interface ICallback {
        void callback_run();
    }

    public interface IPostwork {
        void postwork_run();
    }

    public CommServer(String handlerLocation, String parameter) {
        this.handlerLocation = handlerLocation;
        setParameter(parameter);
        setCycle(-1);
    }

    public CommServer(String handlerLocation, String parameter, int cycle) {
        this.handlerLocation = handlerLocation;
        setParameter(parameter);
        setCycle(cycle);
    }

    public CommServer(String handlerLocation, String parameter, int cycle, boolean noOutput) {
        this(handlerLocation, parameter, cycle);
        this.noOutput = noOutput;
    }

    public int getTIMEOUT() {
        return TIMEOUT;
    }

    public void setTIMEOUT(int TIMEOUT) {
        this.TIMEOUT = TIMEOUT;
    }

    public void setCallback(ICallback callback) {
        this.callback = callback;
    }

    public void setPostwork(IPostwork postwork) {
        this.postwork = postwork;
    }

    public void setCycle(int cycle) {
        this.cycle = cycle;
    }

    public String getResult() {
        return result;
    }

    public void setParameter(String parameter) {
        this.parameter = parameter;
    }

    public void setIpPort(String ip, String port) {
        this.ipAddress = ip;
        this.port = port;
    }

    private void do_work() {
        try {
            if (callback != null) {
                callback.callback_run();
            }

            url = new URL(ServerInfos.HTTP_SERVER_ADDRESS + handlerLocation);
            httpURLConnection = (HttpURLConnection) url.openConnection();

            if (noOutput == true)
                httpURLConnection.setRequestMethod("GET");
            else
                httpURLConnection.setRequestMethod("POST");

            if (noOutput == true)
                httpURLConnection.setDoOutput(false);
            else
                httpURLConnection.setDoOutput(true);

            httpURLConnection.setDoInput(true);

            httpURLConnection.setConnectTimeout(TIMEOUT);
            httpURLConnection.setReadTimeout(TIMEOUT);
            httpURLConnection.setRequestProperty("Accept","*/*");

            if(noOutput == false) {
                outputStream = httpURLConnection.getOutputStream();
                outputStream.write(parameter.getBytes());
                outputStream.close();
            }


            bufferedReader = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
            result = bufferedReader.readLine();
            bufferedReader.close();
            httpURLConnection.disconnect();

            if (postwork != null) {
                postwork.postwork_run();
            }

        } catch (SocketTimeoutException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        if (cycle == -1) {
            while(true) {
                do_work();
            }
        } else {
            for(int i = 1; i <= cycle; i++) {
                do_work();
            }
        }
    }
}