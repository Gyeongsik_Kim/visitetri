package kr.co.alteration.visitetri.activity.convenience.contacts.recycler;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import kr.co.alteration.visitetri.R;

/**
 * Created by GyungDal on 2016-08-23.
 */

public class RecyclerViewHolder extends RecyclerView.ViewHolder  {
    protected ImageView contactImage;
    protected TextView contactName;
    protected TextView contactExtra;
    protected TextView contactMessage;
    protected View container;

    public RecyclerViewHolder(View view) {
        super(view);
        contactImage = (ImageView) view.findViewById(R.id.contact_image);
        contactName = (TextView) view.findViewById(R.id.contact_name);
        contactExtra = (TextView) view.findViewById(R.id.contact_extra_value);
        contactMessage = (TextView) view.findViewById(R.id.contact_send_message_button);
        container = (View) view.findViewById(R.id.contacts_item_view);
    }
}
