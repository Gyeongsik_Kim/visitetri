package kr.co.alteration.visitetri.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import kr.co.alteration.visitetri.R;
import kr.co.alteration.visitetri.activity.convenience.ConvenienceActivity;
import kr.co.alteration.visitetri.activity.find.id.FindIdActivity;
import kr.co.alteration.visitetri.activity.find.pw.FindPwActivity;
import kr.co.alteration.visitetri.activity.mypage.MyPageActivity;
import kr.co.alteration.visitetri.activity.register.RegisterActivity;
import kr.co.alteration.visitetri.activity.register.RegisterAgreeActivity;
import kr.co.alteration.visitetri.comms.CommLogin;
import kr.co.alteration.visitetri.comms.WorkCodes;
import kr.co.alteration.visitetri.device.CookieManager;
import kr.co.alteration.visitetri.device.MyInfo;

public class MainActivity extends AppCompatActivity {
    private EditText editText_id;
    private EditText editText_pw;
    private Button button_login;
    private Button button_register;
    private Button button_find_id;
    private Button button_find_pw;

    private CommLogin commLogin;

    private void startSplash() {
        startActivity(new Intent(MainActivity.this, SplashActivity.class));
    }

    private void allocateView() {
        if (editText_id == null && editText_pw == null && button_login == null && button_register == null && button_find_id == null && button_find_pw == null) {
            editText_id = (EditText) findViewById(R.id.main_input_id);
            editText_pw = (EditText) findViewById(R.id.main_input_pw);
            button_login = (Button) findViewById(R.id.main_login_button);

            button_register = (Button) findViewById(R.id.register_button);
            button_find_pw = (Button) findViewById(R.id.find_pw_button);
            button_find_id = (Button) findViewById(R.id.find_id_button);

            button_register.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(MainActivity.this, RegisterAgreeActivity.class));
                }
            });

            button_find_id.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                    startActivity(new Intent(MainActivity.this, FindIdActivity.class));
                }
            });

            button_find_pw.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(MainActivity.this, FindPwActivity.class));
                    finish();
                }
            });

            button_login.setOnClickListener(new View.OnClickListener() {

                private void failedHandler() {
                    Toast.makeText(MainActivity.this, getText(R.string.unexpected_error), Toast.LENGTH_SHORT).show();
                    finishAffinity();
                }

                @Override
                public void onClick(View v) {
                    try {
                        commLogin = new CommLogin(editText_id.getText().toString(), editText_pw.getText().toString(), MainActivity.this);
                        commLogin.start();
                        commLogin.join();

                        if (commLogin.getResult() == null) {
                            Toast.makeText(MainActivity.this, getText(R.string.cant_login), Toast.LENGTH_SHORT).show();
                        } else {
                            JSONObject result = new JSONObject(commLogin.getResult());
                            if (result.get(WorkCodes.COMM_STATUS).toString().equals("0")) {
                                MyInfo.setMyID(editText_id.getText().toString());
                                CookieManager.setLoginCookie(result.get(WorkCodes.COMM_COOKIE).toString());
                                startActivity(new Intent(MainActivity.this, MyPageActivity.class));
                                finish();
                            } else {
                                Toast.makeText(MainActivity.this, getText(R.string.cant_login), Toast.LENGTH_SHORT).show();
                            }
                        }
                    } catch (InterruptedException e) {
                        failedHandler();
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Splash Activity
        startSplash();
        allocateView();

        //startActivity(new Intent(MainActivity.this, ConvenienceActivity.class));
    }
}
