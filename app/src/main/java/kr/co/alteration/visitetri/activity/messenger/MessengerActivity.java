package kr.co.alteration.visitetri.activity.messenger;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Handler;
import android.os.Message;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import kr.co.alteration.visitetri.R;
import kr.co.alteration.visitetri.activity.messenger.recycler.*;
import kr.co.alteration.visitetri.comms.CommMessenger;
import kr.co.alteration.visitetri.comms.WorkCodes;
import kr.co.alteration.visitetri.device.MyInfo;
import kr.co.alteration.visitetri.image.*;

public class MessengerActivity extends AppCompatActivity {
    private static final int ITEM_VIEW_ME = 0;
    private static final int ITEM_VIEW_NOT_ME = 1;
    private static final int MESSAGE_SINGLE_LINE_MAX = 20;
    private ListView messageListView;
    private ListViewAdapter listViewAdapter;
    private Button messageSend;
    private EditText messageText;
    private Intent intent;

    private CommMessenger commMessenger;

    private Handler addMessageHandler = new Handler() {
        @Override
        public void handleMessage(Message msg){
            JSONObject message = null;
            try {
                message = new JSONObject((String)msg.obj);

                MessageItem item = new MessageItem(ITEM_VIEW_NOT_ME, message.get(WorkCodes.COMM_MESSAGE_SENDER).toString()
                        , message.get(WorkCodes.COMM_MESSAGE).toString(), getTime(),
                        QRCode.getQRCode("I want home"));

                listViewAdapter.addItem(item);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    };

    private void initIntent() {
        if (intent == null) {
            intent = getIntent();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_messenger);
        messageListView = (ListView)findViewById(R.id.message_list);
        listViewAdapter = new ListViewAdapter();
        messageSend = (Button)findViewById(R.id.message_send_button);
        messageText = (EditText)findViewById(R.id.message_text_for_send);
        messageListView.setAdapter(listViewAdapter);
        initIntent();

        try {
            commMessenger = new CommMessenger(addMessageHandler);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        messageSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isEmpty(messageText.getText().toString())){
                    Toast.makeText(getApplicationContext(), "입력되지 않았습니다."
                        ,Toast.LENGTH_SHORT).show();
                    return;
                }
                StringBuilder msg = new StringBuilder(messageText.getText().toString());
                if(msg.length() > MESSAGE_SINGLE_LINE_MAX) {
                    for (int i = 1; i <= msg.length() / MESSAGE_SINGLE_LINE_MAX; i++)
                        msg.insert(i * MESSAGE_SINGLE_LINE_MAX, "\r\n");
                }
                Log.i("Message Text", msg.toString());

                MessageItem item = new MessageItem(ITEM_VIEW_ME, getMyName()
                        , msg.toString(), getTime(),
                        QRCode.getQRCode("I want home"));

                commMessenger.sendMessage(msg.toString(), "test_account");
                listViewAdapter.addItem(item);
                messageText.setText("");
            }
        });

    }
    private boolean isEmpty(String str){
        return (str.isEmpty() || str.trim().isEmpty());
    }

    private String getMyName(){
        return "";
    }

    private String getTime(){
        Calendar cal = Calendar.getInstance(Locale.KOREA);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd, HH:mm");
        return format.format(cal.getTime());
    }
}
