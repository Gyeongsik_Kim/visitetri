package kr.co.alteration.visitetri.activity.find.pw;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.ExecutionException;

import kr.co.alteration.visitetri.R;
import kr.co.alteration.visitetri.comms.CommFindPw;
import kr.co.alteration.visitetri.comms.WorkCodes;

public class FindPwActivity extends AppCompatActivity {
    private EditText nameField;
    private EditText phoneNumberField;
    private EditText birthdayField;
    private Button nextButton;

    private void allocateView() {
        if(nameField == null && phoneNumberField == null && birthdayField == null && nextButton == null) {
            nameField = (EditText) findViewById(R.id.find_pw_name_field);
            phoneNumberField = (EditText) findViewById(R.id.find_pw_phone_field);
            birthdayField = (EditText) findViewById(R.id.find_pw_birth_field);

            nextButton = (Button) findViewById(R.id.find_pw_button);
            nextButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    goToResultActivity();
                }
            });
        }
    }

    private void errorHandler() {
        Toast.makeText(this, getText(R.string.unexpected_error), Toast.LENGTH_LONG).show();
        finishAffinity();
    }

    private String getPassword() {
        String password = null;

        if(nameField != null && phoneNumberField != null && birthdayField != null) {
            try {
                CommFindPw commFindPw = new CommFindPw(nameField.getText().toString(), nameField.getText().toString(), birthdayField.getText().toString(), this);
                commFindPw.start();
                commFindPw.join();

                password = new JSONObject(commFindPw.getResult()).get(WorkCodes.MEMBER_PASSWORD).toString();
            } catch (InterruptedException e) {
                errorHandler();
                e.printStackTrace();
            } catch (JSONException e) {
                errorHandler();
                e.printStackTrace();
            }
        }

        return password;
    }

    private void goToResultActivity() {
        if(nameField != null && phoneNumberField != null && birthdayField != null) {
            String password = getPassword();

            if (password != null) {
                Intent intent = new Intent(FindPwActivity.this, FindPwResultActivity.class);

                intent.putExtra(WorkCodes.MEMBER_NAME, nameField.getText().toString());
                intent.putExtra(WorkCodes.MEMBER_PASSWORD, password);

                startActivity(intent);
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_password);

        allocateView();
    }
}
