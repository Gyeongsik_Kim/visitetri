package kr.co.alteration.visitetri.activity.convenience.contacts;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;

import kr.co.alteration.visitetri.R;
import kr.co.alteration.visitetri.activity.mypage.car.recycler.CarItem;
import kr.co.alteration.visitetri.activity.mypage.car.recycler.RecyclerViewAdapter;

public class ContactsActivity extends AppCompatActivity {
    private RecyclerView contactList;
    private RecyclerViewAdapter recyclerViewAdapter;
    private ArrayList<CarItem> Items;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacts);
        contactList = (RecyclerView)findViewById(R.id.contacts_list_recyclerView);
        Items = new ArrayList<>();
        recyclerViewAdapter = new RecyclerViewAdapter(this, Items);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        contactList = (RecyclerView) findViewById(R.id.car_list_recyclerView);
        contactList.setHasFixedSize(true);
        contactList.setLayoutManager(layoutManager);
        contactList.setAdapter(recyclerViewAdapter);
    }
}
