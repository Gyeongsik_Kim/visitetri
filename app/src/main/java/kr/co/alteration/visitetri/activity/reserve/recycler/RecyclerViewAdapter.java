package kr.co.alteration.visitetri.activity.reserve.recycler;

import android.app.Activity;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import kr.co.alteration.visitetri.R;

/**
 * Created by GyungDal on 2016-08-30.
 */

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewHolder> {
    private Activity activity;
    private ArrayList<ReserveItem> items;

    public RecyclerViewAdapter(Activity activity, ArrayList<ReserveItem> items){
        this.activity = activity;
        this.items = items;
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = activity.getLayoutInflater();
        View view = inflater.inflate(R.layout.activity_reserve, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, final int position) {
        holder.reserveGrade.setText(items.get(position).getGrade());
        holder.reserveLocate.setText(items.get(position).getLocate());
        holder.reserveName.setText(items.get(position).getName());
        holder.reserveTime.setText(items.get(position).getTime());
        holder.reserveNumber.setText(items.get(position).getNumber());
        holder.reserveImage.setImageDrawable(items.get(position).getImage());
        holder.reserveCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Snackbar.make(v, "취소 버튼", Snackbar.LENGTH_SHORT).show();
            }
        });


        holder.reserveChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Snackbar.make(v, "변경 버튼", Snackbar.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return (items != null) ? items.size() : 0;
    }
}
