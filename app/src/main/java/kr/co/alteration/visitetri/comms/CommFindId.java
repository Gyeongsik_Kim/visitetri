package kr.co.alteration.visitetri.comms;

import android.app.Activity;
import android.app.ProgressDialog;

import org.json.JSONException;
import org.json.JSONObject;

import kr.co.alteration.visitetri.R;

/**
 * Created by blogc on 2016-08-19.
 */
public class CommFindId extends CommServer {
    private Activity targetActivity;
    private ProgressDialog progressDialog;

    public CommFindId(String memberName, String memberPhone, Activity activity) {
        super(ServerInfos.SERVER_FINDID_HANDLER, "", 1);

        this.targetActivity = activity;
        JSONObject payload = new JSONObject();

        try {
            payload.put(WorkCodes.MEMBER_NAME, memberName);
            payload.put(WorkCodes.MEMBER_PHONE, memberPhone);
            setParameter(payload.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        setCallback(new ICallback() {
            @Override
            public void callback_run() {
                targetActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressDialog = ProgressDialog.show(targetActivity,"",
                                targetActivity.getText(R.string.please_wait), true);
                    }
                });

            }
        });

        setPostwork(new IPostwork() {
            @Override
            public void postwork_run() {
                targetActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressDialog.dismiss();
                    }
                });
            }
        });
    }
}
