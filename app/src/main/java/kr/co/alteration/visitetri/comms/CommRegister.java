package kr.co.alteration.visitetri.comms;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;

import org.json.JSONException;
import org.json.JSONObject;

import kr.co.alteration.visitetri.R;

/**
 * Created by miffle-laptop on 2016-08-07.
 */
public class CommRegister extends CommServer {
    private Activity targetActivity;
    private ProgressDialog progressDialog;

    public CommRegister(String memberName, String memberID, String memberPassword, String memberBirthday, String memberPhone, Activity activity) {
        super(ServerInfos.SERVER_REGISTER_HANDLER, "", 1);

        this.targetActivity = activity;

        JSONObject payload = new JSONObject();
        try {
            payload.put(WorkCodes.MEMBER_NAME, memberName);
            payload.put(WorkCodes.MEMBER_ID, memberID);
            payload.put(WorkCodes.MEMBER_PASSWORD, memberPassword);
            payload.put(WorkCodes.MEMBER_BIRTHDAY, memberBirthday);
            payload.put(WorkCodes.MEMBER_PHONE, memberPhone);
            setParameter(payload.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
/*
        setCallback(new ICallback() {
            @Override
            public void callback_run() {
                targetActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressDialog = ProgressDialog.show(targetActivity,"",
                                targetActivity.getText(R.string.please_wait), true);
                    }
                });

            }
        });

        setPostwork(new IPostwork() {
            @Override
            public void postwork_run() {
                targetActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressDialog.dismiss();
                    }
                });
            }
        });*/
    }
}
