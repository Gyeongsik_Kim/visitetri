package kr.co.alteration.visitetri.activity.register;

import android.app.DatePickerDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Calendar;
import java.util.Date;

import kr.co.alteration.visitetri.R;
import kr.co.alteration.visitetri.comms.CommRegister;

public class RegisterActivity extends AppCompatActivity {
    private EditText editText_register_name;
    private EditText editText_register_id;
    private EditText editText_register_pw;
    private EditText editText_register_pw_confirm;
    private EditText editText_front_number;
    private EditText editText_middle_number;
    private EditText editText_back_number;
    private EditText editText_register_birth;
    private Button button_birth_set;
    private Calendar calendar;
    private Button button_register;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        setVar();
    }

    private String getPhoneNumber() {
        if (editText_front_number != null && editText_middle_number != null && editText_back_number != null) {
            return editText_front_number.getText().toString() + "-" + editText_middle_number.getText().toString() + "-" + editText_back_number.getText().toString();
        } else {
            return null;
        }
    }

    private boolean isPasswordSame() {
        if (editText_register_pw_confirm != null && editText_register_pw != null) {
            Log.d("Password", editText_register_pw_confirm.getText().toString());
            Log.d("Password", editText_register_pw.getText().toString());
            return editText_register_pw_confirm.getText().toString().equals(editText_register_pw.getText().toString());
        } else {
            return false;
        }
    }

    private void setVar(){
        editText_back_number = (EditText)findViewById(R.id.back_number);
        editText_middle_number = (EditText)findViewById(R.id.middle_number);
        editText_front_number = (EditText)findViewById(R.id.front_number);
        editText_register_name = (EditText)findViewById(R.id.register_name);
        editText_register_id = (EditText)findViewById(R.id.register_id);
        editText_register_pw = (EditText)findViewById(R.id.register_pw);
        editText_register_pw_confirm = (EditText)findViewById(R.id.register_pw_confirm);
        editText_register_birth = (EditText)findViewById(R.id.register_birth);
        button_birth_set = (Button)findViewById(R.id.register_birth_set);
        button_register = (Button)findViewById(R.id.register_button);

        button_birth_set.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar mcurrentDate=Calendar.getInstance();
                int mYear = mcurrentDate.get(Calendar.YEAR);
                int mMonth = mcurrentDate.get(Calendar.MONTH);
                int mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog mDatePicker=new DatePickerDialog(RegisterActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker datepicker,int selectedyear, int selectedmonth, int selectedday) {
                        editText_register_birth
                                .setText(selectedyear + getString(R.string.year)
                                    + selectedmonth + getString(R.string.month)
                                    + selectedday + getString(R.string.day));
                        calendar = Calendar.getInstance();
                        calendar.set(selectedyear, selectedmonth, selectedday);
                    }
                },mYear, mMonth, mDay);
                mDatePicker.setTitle("Select date");
                mDatePicker.show();
            }
        });

        button_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!isPasswordSame()) {
                    Toast.makeText(RegisterActivity.this, "비밀번호가 같지 않습니다.", Toast.LENGTH_LONG).show();
                } else {
                    String phoneNumber = getPhoneNumber();
                    if (editText_register_id != null && editText_register_pw != null &&  editText_register_birth != null &&  phoneNumber != null) {
                        CommRegister commRegister = new CommRegister(editText_register_name.getText().toString(), editText_register_id.getText().toString(),
                                editText_register_pw.getText().toString(), editText_register_birth.getText().toString(), phoneNumber, RegisterActivity.this);

                        commRegister.start();
                        try {
                            commRegister.join();
                        } catch (InterruptedException e) {
                            Toast.makeText(RegisterActivity.this, "회원가입에 실패하였습니다.", Toast.LENGTH_LONG).show();
                            e.printStackTrace();
                        }

                        Toast.makeText(RegisterActivity.this, "회원가입에 성공하였습니다", Toast.LENGTH_LONG).show();
                        finish();
                    } else {
                        Toast.makeText(RegisterActivity.this, "회원가입에 실패하였습니다.", Toast.LENGTH_LONG).show();
                    }
                }}
        });

    }
}
