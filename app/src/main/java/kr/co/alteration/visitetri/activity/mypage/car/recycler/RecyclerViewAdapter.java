package kr.co.alteration.visitetri.activity.mypage.car.recycler;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import kr.co.alteration.visitetri.R;

/**
 * Created by GyungDal on 2016-08-23.
 */
public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewHolder> {
    private Activity activity;
    private ArrayList<CarItem> items;

    public RecyclerViewAdapter(Activity activity, ArrayList<CarItem> items){
        this.activity = activity;
        this.items = items;
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = activity.getLayoutInflater();
        View view = inflater.inflate(R.layout.activity_my_car_recycler, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position) {
        holder.carImage.setImageDrawable(items.get(position).getImage());
        holder.carNumber.setText(items.get(position).getNumber());
        holder.carDelete.setOnClickListener(onClickListener(position));
    }

    private View.OnClickListener onClickListener(final int posittion) {
        return new View.OnClickListener(){
            @Override
            public void onClick(View v){
                //TODO : add car delete code
            }
        };
    }
    @Override
    public int getItemCount() {
        return (items != null) ? items.size() : 0;
    }
}
