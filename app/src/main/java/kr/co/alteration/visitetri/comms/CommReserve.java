package kr.co.alteration.visitetri.comms;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by blogc on 2016-08-24.
 */
public class CommReserve extends CommServer {

    public CommReserve(String target, String date, String time) throws JSONException {
        super(ServerInfos.SERVER_RESERVE_HANDLER, "", 1);

        JSONObject payload = new JSONObject();

        payload.put(WorkCodes.RESERVE_NAME, target);
        payload.put(WorkCodes.RESERVE_DATE, date);
        payload.put(WorkCodes.RESERVE_TIME, time);

        setParameter(payload.toString());
    }
}
