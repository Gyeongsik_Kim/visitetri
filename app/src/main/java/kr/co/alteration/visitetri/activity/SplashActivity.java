package kr.co.alteration.visitetri.activity;

import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import kr.co.alteration.visitetri.R;
import kr.co.alteration.visitetri.comms.ServerInfos;
import kr.co.alteration.visitetri.comms.WorkCodes;

public class SplashActivity extends AppCompatActivity {
    private final int DELAY_TIME = 3000;

    private void splashNow() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                finish();
            }
        }, DELAY_TIME);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        // go to MainActivity
        splashNow();
    }
}
