package kr.co.alteration.visitetri.activity.find.id;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import kr.co.alteration.visitetri.R;
import kr.co.alteration.visitetri.activity.MainActivity;
import kr.co.alteration.visitetri.comms.WorkCodes;

public class FindIdResultActivity extends AppCompatActivity {
    private Intent intent;
    private TextView findIdMessage;
    private Button gotoLoginButton;
    private Button gotofindPw;

    private void allocateView() {
        if(findIdMessage == null && gotoLoginButton == null) {
            findIdMessage = (TextView) findViewById(R.id.find_id_result_msg);
            gotoLoginButton = (Button) findViewById(R.id.find_pw_result_login);
            gotofindPw = (Button)findViewById(R.id.find_id_pw);

            gotoLoginButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                    startActivity(new Intent(FindIdResultActivity.this, MainActivity.class));
                }
            });

            gotofindPw.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                    startActivity(new Intent(FindIdResultActivity.this, MainActivity.class));
                }
            });
        }
    }

    private void initIntent() {
        if (intent == null) {
            intent = getIntent();
        }
    }

    private void unExpectedError() {
        if (findIdMessage != null) {
            findIdMessage.setText(getText(R.string.unexpected_error));
        }
    }

    private String getMemberId() {
        if (intent != null) {
            return intent.getExtras().get(WorkCodes.MEMBER_ID).toString();
        }
        return null;
    }

    private String getMemberName() {
        if (intent != null) {
            return intent.getExtras().get(WorkCodes.MEMBER_NAME).toString();
        }

        return null;
    }


    private void setResultMessage() {
        String memberName = getMemberName();
        String memberId = getMemberId();

        if (memberName == null || memberId == null) {
            unExpectedError();
        }

        String message = memberName + " \n" + getText(R.string.find_id_msg) + " " + memberId;

        if(findIdMessage != null) {
            findIdMessage.setText(message);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_id_result);

        initIntent();
        allocateView();
        setResultMessage();
    }

}
