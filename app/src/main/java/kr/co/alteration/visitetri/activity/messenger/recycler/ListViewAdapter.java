package kr.co.alteration.visitetri.activity.messenger.recycler;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import kr.co.alteration.visitetri.R;


/**
 * Created by GyungDal on 2016-08-31.
 */

public class ListViewAdapter extends BaseAdapter {
    private static final int ITEM_VIEW_ME = 0;
    private static final int ITEM_VIEW_NOT_ME = 1;
    private static final int ITEM_VIEW_MAX = 2;
    private ArrayList<MessageItem> items = new ArrayList<MessageItem>();
    public ListViewAdapter(){

    }
    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public int getViewTypeCount(){
        return this.ITEM_VIEW_MAX;
    }

    @Override
    public int getItemViewType(int position){
        return items.get(position).getType();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public synchronized View getView(int position, View convertView, ViewGroup parent) {
        final Context context = parent.getContext();
        int viewType = getItemViewType(position);

        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            MessageItem item = items.get(position);

            switch(viewType){
                case ITEM_VIEW_ME :{
                    convertView = inflater.inflate(R.layout.activity_messager_me,
                        parent, false);
                    TextView messageName = (TextView) convertView.findViewById(R.id.message_name);
                    TextView messageText = (TextView) convertView.findViewById(R.id.message_text);
                    TextView messageTime = (TextView) convertView.findViewById(R.id.message_time);
                    ImageView messageImage = (ImageView) convertView.findViewById(R.id.message_image);

                    messageName.setText(item.getName());
                    messageText.setText(item.getText());
                    messageTime.setText(item.getTime());
                    messageImage.setImageBitmap(item.getImage());
                    break;
                }

                case ITEM_VIEW_NOT_ME :{
                    convertView = inflater.inflate(R.layout.activity_messenger_other_persion,
                            parent, false);
                    TextView messageName = (TextView) convertView.findViewById(R.id.message_name);
                    TextView messageText = (TextView) convertView.findViewById(R.id.message_text);
                    TextView messageTime = (TextView) convertView.findViewById(R.id.message_time);
                    ImageView messageImage = (ImageView) convertView.findViewById(R.id.message_image);

                    messageName.setText(item.getName());
                    messageText.setText(item.getText());
                    messageTime.setText(item.getTime());
                    messageImage.setImageBitmap(item.getImage());
                    break;
                }
                default:
                    Log.wtf("Message Layout", "What's this type?!?1??!");
                    break;
            }
        }
        return convertView;
    }

    public synchronized void addItem(MessageItem item) {
        Log.i("ListView add", item.getText());
        items.add(item);
        if(items.isEmpty())
            notifyDataSetInvalidated();
        else
            notifyDataSetChanged();
    }
}
