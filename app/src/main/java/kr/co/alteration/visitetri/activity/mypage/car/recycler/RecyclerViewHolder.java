package kr.co.alteration.visitetri.activity.mypage.car.recycler;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import kr.co.alteration.visitetri.R;

/**
 * Created by GyungDal on 2016-08-23.
 */
public class RecyclerViewHolder extends RecyclerView.ViewHolder  {
    protected ImageView carImage;
    protected Button carDelete;
    protected TextView carNumber;
    protected View container;

    public RecyclerViewHolder(View view) {
        super(view);
        carImage = (ImageView) view.findViewById(R.id.car_image);
        carDelete = (Button) view.findViewById(R.id.car_delete_button);
        carNumber = (TextView) view.findViewById(R.id.car_number);
        container = (View) view.findViewById(R.id.car_item_view);

    }
}
