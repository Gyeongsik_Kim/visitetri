package kr.co.alteration.visitetri.comms;

import android.os.Handler;
import android.os.Message;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Vector;

import kr.co.alteration.visitetri.device.MyInfo;

/**
 * Created by blogc on 2016-08-24.
 */
public class CommMessenger {
    private Vector<String> messageVector = new Vector<>();
    private boolean stopReceiveThread = false;
    private Handler messageActivityHandler;

    public CommMessenger(Handler handler) throws JSONException {
        this.messageActivityHandler = handler;
        startReceiveMessage();
    }

    private class SendMessage extends CommServer {
        private JSONObject payload = new JSONObject();

        public SendMessage(String message, String receiver) throws JSONException {
            super(ServerInfos.SERVER_MESSENGER_HANDLER, "", 1);

            payload.put(WorkCodes.COMM_MESSAGE_TYPE, WorkCodes.COMM_MESSAGE_SEND);
            payload.put(WorkCodes.COMM_MESSAGE, message);
            payload.put(WorkCodes.COMM_MESSAGE_RECEIVER, receiver);
            payload.put(WorkCodes.COMM_MESSAGE_SENDER, MyInfo.getMyID());
            payload.put(WorkCodes.MEMBER_ID, MyInfo.getMyID());
            setParameter(payload.toString());
        }
    }

    public boolean sendMessage(String message, String receiver) {
        try {
            SendMessage sendMessage = new SendMessage(message, receiver);
            sendMessage.start();
            sendMessage.join();
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        } catch (InterruptedException e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    private class ReceiveMessage extends CommServer {
        private JSONObject payload = new JSONObject();

        public ReceiveMessage() throws JSONException {
            super(ServerInfos.SERVER_MESSENGER_HANDLER, "");

            payload.put(WorkCodes.COMM_MESSAGE_TYPE, WorkCodes.COMM_MESSAGE_RECEIVE);
            payload.put(WorkCodes.MEMBER_ID, "test_account_1");
            setParameter(payload.toString());

            setPostwork(new IPostwork() {
                @Override
                public void postwork_run() {
                    String result = getResult();

                    if (result != null) {
                        messageVector.add(result);
                        Message message = Message.obtain();
                        message.obj = result.toString();

                        messageActivityHandler.sendMessage(message);
                    }


                    // rest
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    private void startReceiveMessage() throws JSONException {
        ReceiveMessage receiveMessage = new ReceiveMessage();
        receiveMessage.start();
    }
}
