package kr.co.alteration.visitetri.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import kr.co.alteration.visitetri.R;
import kr.co.alteration.visitetri.activity.reserve.ReserveActivity;
import kr.co.alteration.visitetri.comms.WorkCodes;

public class    CenterActivity extends AppCompatActivity {
    private Intent intent;
    private ImageView user_image_imageview;
    private TextView user_info_textview;
    private Button apply_visit_button;
    private Button check_visit_button;

    private void initIntent() {
        if(intent == null) {
            intent = getIntent();
        }
    }

    private void allocateView() {
        if (user_image_imageview == null && user_info_textview == null && apply_visit_button == null) {
            user_image_imageview = (ImageView) findViewById(R.id.center_image_introduce);
            user_info_textview = (TextView) findViewById(R.id.center_textview_introduce);

            apply_visit_button = (Button) findViewById(R.id.center_button_apply_visit);
            check_visit_button = (Button) findViewById(R.id.center_button_reserve_apply);

            apply_visit_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(CenterActivity.this, ReserveActivity.class));
                    finish();
                }
            });

            check_visit_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                }
            });
            //user_info_textview.setText(makeInroduceText());
        }
    }

    private String makeInroduceText() {
        return getText(R.string.name) + ": " + getNameFromIntent() + "\n" + getText(R.string.phone) + ": " + getPhoneFromIntent();
    }

    private String getNameFromIntent() {
        if(intent != null) {
            return intent.getExtras().get(WorkCodes.MEMBER_NAME).toString();
        }

        return null;
    }

    private String getPhoneFromIntent() {
        if (intent != null) {
            return intent.getExtras().get(WorkCodes.MEMBER_PHONE).toString();
        }

        return null;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_center);

        Toast.makeText(this, getText(R.string.success_login), Toast.LENGTH_SHORT).show();
        initIntent();
        allocateView();
    }
}
