package kr.co.alteration.visitetri.comms;

/**
 * Created by miffle-laptop on 2016-08-07.
 */
public class ServerInfos {

    /* Server */
    public static final String SERVER_ADDRESS = "172.30.1.11";
    public static final int SERVER_PORT = 9911;
    public static final String HTTP_SERVER_ADDRESS = "http://" + SERVER_ADDRESS + ":" + SERVER_PORT + "/";
    public static final int SERVER_TIMEOUT = 30000;

    public static final String SERVER_ALIVE_HANDLER = "alive.html";
    public static final String SERVER_FINDPW_HANDLER = "findpw.html";
    public static final String SERVER_FINDID_HANDLER = "findid.html";
    public static final String SERVER_LOGIN_HANDLER = "login.html";
    public static final String SERVER_REGISTER_HANDLER = "register.html";
    public static final String SERVER_MESSENGER_HANDLER = "messenger.html";
    public static final String SERVER_RESERVE_HANDLER = "reserve.html";
}