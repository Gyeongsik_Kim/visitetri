package kr.co.alteration.visitetri.activity.find.id;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.ExecutionException;

import kr.co.alteration.visitetri.R;
import kr.co.alteration.visitetri.comms.CommFindId;
import kr.co.alteration.visitetri.comms.CommFindPw;
import kr.co.alteration.visitetri.comms.WorkCodes;

public class FindIdActivity extends AppCompatActivity {
    private EditText nameField;
    private EditText phoneNumberField;
    private Button nextButton;

    private void allocateView() {
        if(nameField == null && phoneNumberField == null && nextButton == null) {
            nameField = (EditText) findViewById(R.id.find_pw_name_field);
            phoneNumberField = (EditText) findViewById(R.id.find_pw_phone_field);

            nextButton = (Button) findViewById(R.id.find_pw_button);
            nextButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    goToResultActivity();
                }
            });
        }
    }

    private void goToResultActivity() {
        String id = getId();

        if (id != null) {
            Intent intent = new Intent();

            intent.putExtra(WorkCodes.MEMBER_NAME, nameField.getText().toString());
            intent.putExtra(WorkCodes.MEMBER_PHONE, phoneNumberField.getText().toString());

            startActivity(intent);
        } else {
            errorHandler();
        }
    }

    private String getId() {
        String id = null;

        if(nameField != null && phoneNumberField != null) {
            try {
                CommFindId commFindId = new CommFindId(nameField.getText().toString(), nameField.getText().toString(), this);
                commFindId.start();
                commFindId.join();
                id = new JSONObject(commFindId.getResult()).get(WorkCodes.MEMBER_ID).toString();
            } catch (InterruptedException e) {
                errorHandler();
                e.printStackTrace();
            } catch (JSONException e) {
                errorHandler();
                e.printStackTrace();
            }
        }

        return id;
    }

    private void errorHandler() {
        Toast.makeText(this, getText(R.string.unexpected_error), Toast.LENGTH_LONG).show();
        finishAffinity();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_id);

        allocateView();
    }
}
