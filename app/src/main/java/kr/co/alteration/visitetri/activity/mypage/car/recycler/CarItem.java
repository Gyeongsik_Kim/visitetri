package kr.co.alteration.visitetri.activity.mypage.car.recycler;

import android.graphics.drawable.Drawable;

/**
 * Created by GyungDal on 2016-08-23.
 */
public class CarItem {
    private String number;
    private Drawable image;

    public CarItem(String number, Drawable image){
        this.number = number;
        this.image = image;
    }

    public String getNumber(){
        return this.number;
    }

    public Drawable getImage(){
        return this.image;
    }

    public void setNumber(String number){
        this.number = number;
    }

    public void setImage(Drawable image){
        this.image = image;
    }
}
