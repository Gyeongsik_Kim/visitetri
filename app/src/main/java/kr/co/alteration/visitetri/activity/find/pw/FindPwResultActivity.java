package kr.co.alteration.visitetri.activity.find.pw;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import kr.co.alteration.visitetri.R;
import kr.co.alteration.visitetri.activity.MainActivity;
import kr.co.alteration.visitetri.comms.WorkCodes;

public class FindPwResultActivity extends AppCompatActivity {
    private Intent intent;
    private TextView findPwMessage;
    private Button gotoLoginButton;

    private void allocateView() {
        if(findPwMessage == null && gotoLoginButton == null) {
            findPwMessage = (TextView) findViewById(R.id.find_pw_result_msg);
            gotoLoginButton = (Button) findViewById(R.id.find_pw_result_login);

            gotoLoginButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    FindPwResultActivity.this.finish();
                    startActivity(new Intent(FindPwResultActivity.this, MainActivity.class));
                }
            });
        }
    }

    private void initIntent() {
        if (intent == null) {
            intent = getIntent();
        }
    }

    private String getMemberPw() {
        if (intent != null) {
            return intent.getExtras().get(WorkCodes.MEMBER_PASSWORD).toString();
        }
        return null;
    }

    private String getMemberName() {
        if (intent != null) {
            return intent.getExtras().get(WorkCodes.MEMBER_NAME).toString();
        }

        return null;
    }

    private void unExpectedError() {
        if (findPwMessage != null) {
            findPwMessage.setText(getText(R.string.unexpected_error));
        }
    }

    private void setResultMessage() {
        String memberName = getMemberName();
        String memberPw = getMemberPw();

        if (memberName == null || memberPw == null) {
            unExpectedError();
        }

        String message = memberName + " \n" + getText(R.string.find_pw_msg) + " " + memberPw;

        if(findPwMessage != null) {
            findPwMessage.setText(message);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_password_result);

        initIntent();
        allocateView();
        setResultMessage();
    }
}
