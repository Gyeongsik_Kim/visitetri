package kr.co.alteration.visitetri.activity.messenger.recycler;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by GyungDal on 2016-08-31.
 */
//TODO : not complete
public class MessageItem {
    private int type;
    private String name;
    private String text;
    private String time;
    private Bitmap image;
    public MessageItem(int type, String name, String text, String time, Bitmap image){
        this.type = type;
        this.name = name;
        this.text = text;
        this.time = time;
        this.image = image;
    }

    public int getType(){
        return this.type;
    }

    public String getName(){
        return this.name;
    }

    public String getText(){
        return this.text;
    }

    public String getTime(){
        return this.time;
    }

    public Bitmap getImage(){
        return this.image;
    }
}
