package kr.co.alteration.visitetri.comms;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import kr.co.alteration.visitetri.R;

/**
 * Created by blogc on 2016-08-19.
 */
public class CommFindPw extends CommServer {
    private Activity targetActivity;
    private ProgressDialog progressDialog;

    public CommFindPw(String memberName, String memberPhone, String birthday, Activity activity) {
        super(ServerInfos.SERVER_FINDPW_HANDLER, "", 1);

        this.targetActivity = activity;
        JSONObject payload = new JSONObject();

        try {
            payload.put(WorkCodes.MEMBER_NAME, memberName);
            payload.put(WorkCodes.MEMBER_PHONE, memberPhone);
            payload.put(WorkCodes.MEMBER_BIRTHDAY, birthday);
            setParameter(payload.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        setCallback(new CommServer.ICallback() {
            @Override
            public void callback_run() {
                targetActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressDialog = ProgressDialog.show(targetActivity,"",
                                targetActivity.getText(R.string.please_wait), true);
                    }
                });

            }
        });

        setPostwork(new CommServer.IPostwork() {
            @Override
            public void postwork_run() {
                targetActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressDialog.dismiss();
                    }
                });
            }
        });
    }
}
