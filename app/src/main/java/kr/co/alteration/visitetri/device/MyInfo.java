package kr.co.alteration.visitetri.device;

/**
 * Created by blogc on 2016-09-12.
 */
public class MyInfo {

    private static String myID;

    public static synchronized void setMyID(String id) {
        myID = id;
    }

    public static synchronized String getMyID() {
        return myID;
    }
}
