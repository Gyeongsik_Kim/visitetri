package kr.co.alteration.visitetri.activity.convenience.contacts.recycler;

import android.graphics.drawable.Drawable;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by GyungDal on 2016-08-23.
 */
//TODO : not complete
public class ContactItem {
    private String name;
    private String extra;
    private String number;
    private Drawable image;
    private boolean hasNumber;
    private static final String NormalCallRegex = "^\\d{2,3}-\\d{3,4}-\\d{4}$";
    private static final String PersonalCallRegex = "^01(?:0|1|[6-9])-(?:\\d{3}|\\d{4})-\\d{4}$";
    public ContactItem(String name, String extra, Drawable image){
        this.name = name;
        this.extra = extra;
        this.image = image;
        getNumberFromExtra(extra);
    }

    public void setNumber(String number){
        this.number = number;
    }

    public void setName(String name){
        this.name = name;
    }

    public void setExtra(String extra){
        this.extra = extra;
    }

    public void setImage(Drawable image){
        this.image = image;
    }

    public boolean getHasNumber(){
        return this.hasNumber;
    }

    public String getNumber(){
        return this.number;
    }

    public String getName(){
        return this.name;
    }

    public String getExtra(){
        return this.extra;
    }

    public Drawable getImage(){
        return this.image;
    }

    private String getNumberFromExtra(String extra){
        Pattern pattern = Pattern.compile(PersonalCallRegex);
        Matcher matcher = pattern.matcher(extra);
        String temp = matcher.replaceAll("");
        if(!temp.isEmpty()){
            hasNumber = true;
            number = temp;
            return temp;
        }

        pattern = Pattern.compile(NormalCallRegex);
        matcher = pattern.matcher(extra);
        temp = matcher.replaceAll("");
        if(!temp.isEmpty()){
            hasNumber = true;
            number = temp;
            return temp;
        }

        hasNumber = false;
        return null;
    }
}
