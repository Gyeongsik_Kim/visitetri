package kr.co.alteration.visitetri.activity.reserve.recycler;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import kr.co.alteration.visitetri.R;

/**
 * Created by GyungDal on 2016-08-30.
 */

public class RecyclerViewHolder extends RecyclerView.ViewHolder  {
    protected ImageView reserveImage;
    protected TextView reserveName;
    protected TextView reserveGrade;
    protected TextView reserveLocate;
    protected TextView reserveNumber;
    protected TextView reserveTime;
    protected Button reserveCancel;
    protected Button reserveChange;
    protected View container;

    public RecyclerViewHolder(View view) {
        super(view);
        reserveImage = (ImageView)view.findViewById(R.id.reserve_image);
        reserveName = (TextView)view.findViewById(R.id.reserve_name);
        reserveGrade = (TextView)view.findViewById(R.id.reserve_grade);
        reserveLocate = (TextView)view.findViewById(R.id.reserve_locate);
        reserveNumber = (TextView)view.findViewById(R.id.reserve_number);
        reserveTime = (TextView)view.findViewById(R.id.reserve_time);
        reserveCancel = (Button) view.findViewById(R.id.reserve_cancel_button);
        reserveChange = (Button) view.findViewById(R.id.reserve_change_button);
        container = (View) view.findViewById(R.id.contacts_item_view);
    }
}
