package kr.co.alteration.visitetri.activity.convenience;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;

import kr.co.alteration.visitetri.R;

public class ConvenienceActivity extends AppCompatActivity {
    private Button button_lost_tag;
    private Button button_contact;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_convenience);
        button_lost_tag = (Button) findViewById(R.id.lost_etri_tag_button);
        button_contact = (Button)findViewById(R.id.contact_button);
    }
}
