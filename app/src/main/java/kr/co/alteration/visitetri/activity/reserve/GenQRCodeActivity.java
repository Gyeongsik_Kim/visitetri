package kr.co.alteration.visitetri.activity.reserve;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

import kr.co.alteration.visitetri.R;
import kr.co.alteration.visitetri.comms.WorkCodes;
import kr.co.alteration.visitetri.image.QRCode;

public class GenQRCodeActivity extends AppCompatActivity {
    private Intent intent;
    private ImageView qrCodeView;

    private void setImageViewWithQRCode() {
        if (qrCodeView != null && intent != null) {
            qrCodeView.setImageBitmap(QRCode.getQRCode(intent.getExtras().get(WorkCodes.COMM_URL).toString()));
        }
    }

    private void allocateView() {
        if (qrCodeView == null) {
            qrCodeView = (ImageView) findViewById(R.id.find_id_result_image);
        }
    }

    private void initIntent() {
        if (intent == null) {
            intent = new Intent();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gen_qrcode);

        initIntent();
        allocateView();
        setImageViewWithQRCode();
    }
}
