package kr.co.alteration.visitetri.activity.reserve.recycler;

import android.graphics.drawable.Drawable;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by GyungDal on 2016-08-30.
 */
public class ReserveItem {
    private String name;
    private String grade;
    private String locate;
    private String number;
    private String time;
    private Drawable image;
    public ReserveItem(String name, String grade
            , String extra, String number, String time, Drawable image){
        this.name = name;
        this.grade = grade;
        this.locate = extra;
        this.number = number;
        this.time = time;
        this.image = image;
    }

    public void setNumber(String number){
        this.number = number;
    }

    public void setName(String name){
        this.name = name;
    }

    public void setLocate(String locate){
        this.locate = locate;
    }

    public void setGrade(String grade){
        this.grade = grade;
    }

    public void setTime(String time){
        this.time = time;
    }

    public void setImage(Drawable image){
        this.image = image;
    }

    public String getNumber(){
        return this.number;
    }

    public String getName(){
        return this.name;
    }

    public String getLocate(){
        return this.locate;
    }

    public String getGrade(){
        return this.grade;
    }

    public String getTime(){
        return this.time;
    }

    public Drawable getImage(){
        return this.image;
    }

}
