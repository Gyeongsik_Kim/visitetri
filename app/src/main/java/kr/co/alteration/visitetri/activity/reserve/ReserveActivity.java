package kr.co.alteration.visitetri.activity.reserve;

import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;

import kr.co.alteration.visitetri.R;
import kr.co.alteration.visitetri.activity.reserve.recycler.RecyclerViewAdapter;
import kr.co.alteration.visitetri.activity.reserve.recycler.ReserveItem;

public class ReserveActivity extends AppCompatActivity {
    private RecyclerView reserveList;
    private RecyclerViewAdapter recyclerViewAdapter;
    private ArrayList<ReserveItem> testItems;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reserve);

        setTestItems();
        recyclerViewAdapter = new RecyclerViewAdapter(this, testItems);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        reserveList = (RecyclerView) findViewById(R.id.reserve_recycler);
        reserveList.setHasFixedSize(true);
        reserveList.setLayoutManager(layoutManager);
        reserveList.setAdapter(recyclerViewAdapter);
    }

    private void setTestItems() {
        testItems = new ArrayList<>(4);
        testItems.add(new ReserveItem("김경식", "잉여", "집에가고싶음", "TEL) 몰라", "내일", ResourcesCompat.getDrawable(getResources(), R.drawable.etri_logo, null)));
        testItems.add(new ReserveItem("김경식", "잉여", "집에가고싶음", "TEL) 몰라", "내일", ResourcesCompat.getDrawable(getResources(), R.drawable.etri_logo, null)));
        testItems.add(new ReserveItem("김경식", "잉여", "집에가고싶음", "TEL) 몰라", "내일", ResourcesCompat.getDrawable(getResources(), R.drawable.etri_logo, null)));
        testItems.add(new ReserveItem("김경식", "잉여", "집에가고싶음", "TEL) 몰라", "내일", ResourcesCompat.getDrawable(getResources(), R.drawable.etri_logo, null)));
    }
}
