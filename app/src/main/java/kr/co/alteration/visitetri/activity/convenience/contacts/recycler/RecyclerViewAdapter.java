package kr.co.alteration.visitetri.activity.convenience.contacts.recycler;

import android.app.Activity;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;

import kr.co.alteration.visitetri.R;

/**
 * Created by GyungDal on 2016-08-23.
 */

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewHolder> {
    private Activity activity;
    private ArrayList<ContactItem> items;

    public RecyclerViewAdapter(Activity activity, ArrayList<ContactItem> items){
        this.activity = activity;
        this.items = items;
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = activity.getLayoutInflater();
        View view = inflater.inflate(R.layout.activity_contacts, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, final int position) {
        holder.contactImage.setImageDrawable(items.get(position).getImage());
        holder.contactName.setText(items.get(position).getName());
        holder.contactExtra.setText(items.get(position).getExtra());
        holder.contactMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(items.get(position).getHasNumber()){
                    Intent sendIntent = new Intent(Intent.ACTION_VIEW);
                    sendIntent.putExtra("sms_body", ""); // 보낼 문자
                    sendIntent.putExtra("address", items.get(position).getNumber()); // 받는사람 번호
                    sendIntent.setType("vnd.android-dir/mms-sms");
                    activity.startActivity(sendIntent);
                }else
                    Snackbar.make(v
                            , activity.getString(R.string.not_found_number)
                            , Snackbar.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return (items != null) ? items.size() : 0;
    }
}
