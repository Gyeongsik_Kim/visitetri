package kr.co.alteration.visitetri.activity.mypage.car;

import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;

import kr.co.alteration.visitetri.R;
import kr.co.alteration.visitetri.activity.mypage.car.recycler.CarItem;
import kr.co.alteration.visitetri.activity.mypage.car.recycler.RecyclerViewAdapter;

public class MyCarActivity extends AppCompatActivity {
    private RecyclerView carListView;
    private RecyclerViewAdapter recyclerViewAdapter;
    private ArrayList<CarItem> testItems;

    //TODO : 현재 작동 테스트용 코드 제거 및 자동차 얻어오는 쪽 소스 추가
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_car);
        setTestItems();
        recyclerViewAdapter = new RecyclerViewAdapter(this, testItems);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        carListView = (RecyclerView) findViewById(R.id.car_list_recyclerView);
        carListView.setHasFixedSize(true);
        carListView.setLayoutManager(layoutManager);
        carListView.setAdapter(recyclerViewAdapter);
    }

    private void setTestItems() {
        testItems = new ArrayList<>(4);
        testItems.add(new CarItem("23정 1242", ResourcesCompat.getDrawable(getResources(), R.drawable.etri_logo, null)));
        testItems.add(new CarItem("23의 4142", ResourcesCompat.getDrawable(getResources(), R.drawable.etri_logo, null)));
        testItems.add(new CarItem("54구 5321", ResourcesCompat.getDrawable(getResources(), R.drawable.etri_logo, null)));
        testItems.add(new CarItem("29현 8956", ResourcesCompat.getDrawable(getResources(), R.drawable.etri_logo, null)));
    }
}
