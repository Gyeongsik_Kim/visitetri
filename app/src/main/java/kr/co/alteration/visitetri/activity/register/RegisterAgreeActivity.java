package kr.co.alteration.visitetri.activity.register;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Toast;

import kr.co.alteration.visitetri.R;

public class RegisterAgreeActivity extends AppCompatActivity {
    private CheckBox agreeCheck;
    private Button nextButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_agree);
        agreeCheck = (CheckBox)findViewById(R.id.agree_check);
        nextButton = (Button) findViewById(R.id.next_button);

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(agreeCheck.isChecked()){
                    startActivity(new Intent(RegisterAgreeActivity.this, RegisterActivity.class));
                    finish();
                }else{
                    Toast.makeText(getApplication(), getString(R.string.plz_agree), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
