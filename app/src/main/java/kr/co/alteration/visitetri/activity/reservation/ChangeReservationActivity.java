package kr.co.alteration.visitetri.activity.reservation;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import kr.co.alteration.visitetri.R;

/**
 * Created by GyungDal on 2016-09-20.
 */
public class ChangeReservationActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_reservation);
    }
}
