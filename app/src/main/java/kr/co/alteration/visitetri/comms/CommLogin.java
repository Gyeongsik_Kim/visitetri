package kr.co.alteration.visitetri.comms;

import android.app.Activity;
import android.app.ProgressDialog;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by blogc on 2016-08-21.
 */
public class CommLogin extends CommServer {
    private ProgressDialog progressDialog;
    private Activity targetActivity;

    public CommLogin(String memberID, String memberPassword, Activity activity) {
        super(ServerInfos.SERVER_LOGIN_HANDLER, "", 1);
        this.targetActivity = activity;

        JSONObject payload = new JSONObject();
        try {
            payload.put(WorkCodes.MEMBER_ID, memberID);
            payload.put(WorkCodes.MEMBER_PASSWORD, memberPassword);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        setParameter(payload.toString());
    }
}
