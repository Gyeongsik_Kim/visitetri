package kr.co.alteration.visitetri.image;

import android.graphics.Bitmap;


import java.io.ByteArrayOutputStream;

/**
 * Created by blogc on 2016-08-24.
 */
public class QRCode {

    public static Bitmap getQRCode(String url) {
        return net.glxn.qrgen.android.QRCode.from(url).bitmap();
    }
}
